# Guessing game  #

A simple number guessing game. Made by following [the official Rust book][1].
This project was used to practice Rust and Git.

## Setting up ##

You can build this project using Cargo. Run `cargo build` and the executable
can be found in target/debug. Or run `cargo run` to build and run with one
line.

The dependencies are automatically updated by Cargo.


[1]: https://doc.rust-lang.org/book
