extern crate rand;

use rand::Rng;
use std::cmp::Ordering;
use std::io;

fn main() {
    // Welcome the player.
    println!("Welcome to the guessing game.");

    // Create a secret number.
    let number = rand::thread_rng().gen_range(1, 101);

    loop {
        // Let the player guess.
        println!("Please input your guess.");
        let mut guess = String::new();
        io::stdin().read_line(&mut guess)
                   .expect("Failed to read the guess!");

        // Convert the guess to a number.
        let guess:u32 = match guess.trim().parse() {
            Ok(num) => num,
            Err(_)  => continue,
        };
        println!("You guessed {}.", guess);

        // Deal with the guess.
        print!("Your guess was ");
        match guess.cmp(&number) {
            Ordering::Less    => println!("too small."),
            Ordering::Greater => println!("too big."),
            Ordering::Equal   => {
                println!("correct!");
                break;
            }
        }
    }
}
